# Jpa-Audit
A **Java** library that must be used with **Spring Boot**. It will monitor the owners of changes in the database.
# Usage
In your pom.xml use:

``` xml
<dependency>
    <groupId>com.br.t2b.jpa.audit</groupId>
    <artifactId>jpa-audit</artifactId>
    <version>0.0.5-SNAPSHOT</version>
</dependency>
```

and

``` xml
<repositories>
   <repository>
     <id>jitpack.io</id>
     <url>https://jitpack.io</url>
   </repository>
</repositories>
```

# Generating another version

1 - Create a new branch from the master branch.

2 - Make your modifications.

3 - Update the **pom.xml** version section, the **README.md** file, tag your last commit with the new version and push everything.

4 - Merge the code into the master branch. Check the permissions to do it.

5 - Create tag from de master branch whith name version from pom.xml.
